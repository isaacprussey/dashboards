import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { ContentContainer } from '../_components';
import { sectionConstants } from '../_constants';

class WelcomePage extends React.Component {

    componentDidMount(){
        const { user, activedashboard, dispatch } = this.props;
        
        if (user && activedashboard) {
            dispatch(userActions.welcome(user, activedashboard));
        }
    }

    componentWillReceiveProps(nextProps){
        //const { user, activedashboard} = this.props;
        if(nextProps && nextProps.activesection && nextProps.activesection !== sectionConstants.NOTSET && nextProps.activesection !== sectionConstants.WELCOME){
            //
        }
    }

    render() {
        const { welcome } = this.props;
        return (
            <ContentContainer>  
                <div id="welcome-page" className="landing-page" >
                <div className="mainheadertext">
                    <div className="headertext">
                       <span>{welcome && welcome.data}</span>
                    </div>
                </div>
                </div>
            </ContentContainer>
        );
    }
}


function mapStateToProps(state) {
    const { authentication, activedashboard, welcome, className, dispatch, activesection, dashboard } = state;
    const { user } = authentication;
    return {
        user, activedashboard, welcome, className, dispatch, activesection, dashboard
    };
}

const connectedWelcomePage = connect(mapStateToProps)(WelcomePage);
export { connectedWelcomePage as WelcomePage };