import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { ContentContainer } from '../_components';

class DashboardPage extends React.Component {

    constructor(Props){
        super(Props);
        this.state = {offsetHeight:0}
    }

    componentDidMount(){
        const { user, activedashboard, dispatch } = this.props;

        if (user && activedashboard) {
            dispatch(userActions.dashboard(user, activedashboard));
        }
    }
    
    render() {
        const { dashboard } = this.props;
        return (
            <ContentContainer className="dashboard">
                <div id="dashboard-page" className="landing-page" >
                <div className="mainheadertext">
                    <div className="headertext">
                       <span>{dashboard && dashboard.data}</span>
                    </div>
                </div>
                </div>    
            </ContentContainer>
        );
    }
}

function mapStateToProps(state) {
    const { authentication, activedashboard, dashboard, className, offsetHeight } = state;
    const { user } = authentication;
    return {
        user, activedashboard, dashboard, className, offsetHeight
    };
}

const connectedDashboardPage = connect(mapStateToProps)(DashboardPage);
export { connectedDashboardPage as DashboardPage };