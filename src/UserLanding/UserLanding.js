import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { CustomFullPageContainer } from '../_components';
import { LeftNavMenu, WelcomePage, DashboardPage } from '../UserLanding';

class UserLanding extends React.Component {

    constructor(Props){
        super(Props);
        this.state = {
            current: 0,
            fullpageoptions: {
                delay: 900,
                verticalAlign: false,
                scrollBar: false,
                className: 'FullPageContainer',
                sectionClassName: 'Section',
                anchors: this.createMenuList(this.props.activedashboard),
                activeClass: 'active',
                sectionPaddingTop: '0',
                sectionPaddingBottom: '0',
                arrowNavigation: true,
                activeSection: 0,
                touchNavigation: true,
                mouseWheelNavigation: true,
                scrollCallback: (states) => this.setState({current: states.activeSection})
            }
        }
    }
    
    createMenuList(activedashboard){
        return ['welcome', 'dashboard'];
    }

    componentWillReceiveProps(nextProps)
    {
        const that = this;
        const { activedashboard } = nextProps;
        that.setState({
            fullpageoptions: {
                delay: 900,
                verticalAlign: false,
                scrollBar: false,
                className: 'FullPageContainer',
                sectionClassName: 'Section',
                anchors: that.createMenuList(activedashboard),
                activeClass: 'active',
                sectionPaddingTop: '0',
                sectionPaddingBottom: '0',
                arrowNavigation: true,
                activeSection: 0,
                mouseWheelNavigation: true,
                scrollCallback: (states) => this.setState({current: states.activeSection})
            }
        });
    }

    render() {
        const { user, location } = this.props;
        const { fullpageoptions } = this.state;
        const {current} = this.state
        return (
            <div id="userlanding" className='userlanding'>
            {user && <LeftNavMenu />}
            <CustomFullPageContainer {...fullpageoptions} activeSection={current}>                  
                <WelcomePage />
                {user && authCheck.passUserInspection(user) === true
                 ? <DashboardPage />
                 : <Redirect to={{ pathname: '/login', state: { from: location } }} />}
            </CustomFullPageContainer>
            </div>
        );
    }
}

const authCheck = {
    passUserInspection(user) {
        if(user){
            if(user && user.loggedIn === true) return true;
            return false
        } 
        return false
    }
}

function mapStateToProps(state) {
    const { authentication, activedashboard, className, dispatch, activesection } = state;
    const { user } = authentication;
    return {
        user, activedashboard, className, dispatch, activesection
    };
}

const connectedUserLanding = connect(mapStateToProps)(UserLanding);
export { connectedUserLanding as UserLanding };