import React from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

class LeftNavMenu extends React.Component{
    
    render(){
        const overlayStyle = {...defaultStyles.overlay, ...this.props.styles.overlay};

        return(
                <div>
                <div className="overlay"
                    style={overlayStyle}
                    role="presentation"
                    tabIndex="0"
                />
                <nav className="main-menu" id="main-menu" ref="LeftNavMenu" >
                    <a href="#welcome" style={{ textDecoration: 'none' }}>
                                <div style={{textAlign: 'center'}}>
                                    <img style={{height: '60px', width: 'auto'}} className="icon" alt="Welcome" src="icons8-door-opened-50.png"  />
                                    <span style={{color: 'white', fontSize: '16px', fontFamily: 'sarif'}}>Welcome</span>
                                </div>
                    </a>
                    <ul className="nav">
                        <hr/>
                        <div>
                            <a href="#dashboard" style={{ textDecoration: 'none', minHeight: '30px' }}>
                                <div style={{textAlign: 'center'}}>
                                    <img style={{height: '60px', width: 'auto'}} className="icon" alt="Dashboard" src="icons8-speed-64.png"  />
                                    <span style={{ color: 'white', fontSize: '16px', fontFamily: 'sarif'}}>Dashboard</span>
                                </div> 
                            </a>  
                        </div>
                        <hr/>
                        <li className="has-subnav">
                            <Link to="/login" style={{textDecoration: 'none', minHeight: '30px' }}>
                                <div style={{textAlign: 'center'}}>
                                    <img style={{height: '60px', width: 'auto'}} className="icon" alt="Logout" src="icons8-exit-50.png"  />
                                    <span style={{color: 'white', fontSize: '16px', fontFamily: 'sarif'}}>Logout</span>
                                </div> 
                            </Link>
                        </li>
                    </ul>
                </nav>
                </div>
        );
    }
}

const defaultStyles = {
    leftnavmenu: {
      zIndex: 2,
      position: 'absolute',
      top: 0,
      bottom: 0,
      transition: 'transform .3s ease-out',
      WebkitTransition: '-webkit-transform .3s ease-out',
      willChange: 'transform',
      overflowY: 'auto',
    },
    root: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        overflow: 'hidden',
    },
    overlay: {
        zIndex: 1,
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        opacity: 1,
        display: 'none',
        height: '100%',
        width: '100%',
        transition: 'opacity .3s ease-out, visibility .3s ease-out',
        backgroundColor: 'rgba(0,0,0,.3)',
    },
  };
  
LeftNavMenu.defaultProps = {
    open: false,
    transitions: true,
    shadow: true,
    onSetOpen: () => {},
    styles: {},
    defaultLeftNavMenuWidth: 0,
  };

function mapStateToProps(state) {
    const { authentication, activedashboard, activesection  } = state;
    const { user } = authentication;
    return {
        user, activedashboard, activesection
    };
}

const connectedLeftNavMenu = connect(mapStateToProps)(LeftNavMenu);
export { connectedLeftNavMenu as LeftNavMenu };