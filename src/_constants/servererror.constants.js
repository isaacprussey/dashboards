export const serverErrorConstants = {
    ErrorMessage401: {error:"Request failed with status code 401", message:"The Username or Password you entered is incorrect"},
    ErrorMessage404: {error:"Request failed with status code 404", message:"User Not Found"},
    ErrorMessage500: {error:"Request failed with status code 500", message:"An unexpected internal server error occurred"},
    ErrorMessage503: {error:"Request failed with status code 503", message:"The resource center is temporarily unavailable, Please try again later"},
    ErrorMessage504: {error:"Request failed with status code 504", message:"Session Timeout, Please login again"},
    ErrorMessageConnection: {error:"Network Error", message:"Please check your internet connection"},
};

export const errors = [serverErrorConstants.ErrorMessage401, serverErrorConstants.ErrorMessage404, serverErrorConstants.ErrorMessage500, serverErrorConstants.ErrorMessage503, serverErrorConstants.ErrorMessage504, serverErrorConstants.ErrorMessageConnection];