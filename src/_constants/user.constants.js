export const userConstants = {
    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    
    WELCOME_REQUEST: 'USERS_WELCOME_REQUEST',
    WELCOME_SUCCESS: 'USERS_WELCOME_SUCCESS',
    WELCOME_FAILURE: 'USERS_WELCOME_FAILURE',
    WELCOME_CLEAR: 'USERS_WELCOME_CLEAR',

    DASHBOARD_REQUEST: 'USERS_DASHBOARD_REQUEST',
    DASHBOARD_SUCCESS: 'USERS_DASHBOARD_SUCCESS',
    DASHBOARD_FAILURE: 'USERS_DASHBOARD_FAILURE',
    DASHBOARD_CLEAR: 'USERS_DASHBOARD_CLEAR',

    ACTIVESECTION_REQUEST: 'USERS_ACTIVESECTIONE_REQUEST',
    ACTIVESECTION_SUCCESS: 'USERS_ACTIVESECTION_SUCCESS',
    ACTIVESECTION_FAILURE: 'USERS_ACTIVESECTION_FAILURE',
    ACTIVESECTION_CLEAR: 'USERS_ACTIVESECTION_CLEAR',

    LOGOUT: 'USERS_LOGOUT', 
};