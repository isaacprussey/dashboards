import * as React from 'react';
import {sectionActions} from '../_actions';
import { connect } from 'react-redux';

class CustomFullPageContainer extends React.Component {
    _resetScrollTimer;
    _childrenLength;

    constructor(props) {
        super(props);
        this.state = {
            activeSection: props.activeSection,
            scrollingStarted: false,
            sectionScrolledPosition: 0,
            windowHeight: 0,
            delay: props.delay,
        };

        this._handleMouseWheel = this._handleMouseWheel.bind(this);
        this._handleAnchor = this._handleAnchor.bind(this);
        this._handleResize = this._handleResize.bind(this);
        this._handleArrowKeys = this._handleArrowKeys.bind(this);
        this.onMove = this.onMove.bind(this);
    }

    componentWillUnmount() {
        this._clearResetScrollTimer();
        this._removeDefaultEventListeners();
        this._removeMouseWheelEventHandlers();
        this._removeOverflowFromBody();
        //this.onMove();
    }

    componentDidMount() {
        this._childrenLength = this.props.children.length;
        this.onMove();
        this._handleResize();
        window.addEventListener('resize', this._handleResize);

        if (!this.props.scrollBar) {
            this._addCSS3Scroll();
            this._handleAnchor();

            window.addEventListener('hashchange', this._handleAnchor, false);

            if (this.props.arrowNavigation) {
                window.addEventListener('keydown', this._handleArrowKeys);
            }
            if (this.props.touchNavigation) {
              this._handleTouchNav();
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch } = this.props;
        if(this.props.activeSection !== nextProps.activeSection && nextProps.anchors) {
            this.setState({activeSection: nextProps.activeSection})

            var currentAnchor = nextProps.anchors[nextProps.activeSection];

            // we are setting the active section here - adding it to the redux store
            dispatch(sectionActions.set(currentAnchor));

            this._setAnchor(nextProps.activeSection);
            this._handleSectionTransition(nextProps.activeSection);
            this._addActiveClass();
      }
    }

    _removeDefaultEventListeners() {
        window.removeEventListener('resize', this._handleResize);
        window.removeEventListener('hashchange', this._handleAnchor);

        if (this.props.arrowNavigation) {
            window.removeEventListener('keydown', this._handleArrowKeys);
        }
    }

    _addCSS3Scroll() {
        this._addOverflowToBody();
        if(this.props.mouseWheelNavigation){
            this._addMouseWheelEventHandlers();
        }
    }

    _addActiveClass() {
        this._removeActiveClass();

        let hash = window.location.hash.substring(1);
        let activeLinks = document.querySelectorAll(`a[href="#${hash}"]`);

        for (let i = 0; i < activeLinks.length; i++) {
            activeLinks[i].className = activeLinks[i].className + (activeLinks[i].className.length > 0 ? ' ' : '') + `${this.props.activeClass}`;
        }
    }

    _removeActiveClass() {
        let activeLinks = document.querySelectorAll(`a:not([href="#${this.props.anchors[this.state.activeSection]}"])`);

        for (let i = 0; i < activeLinks.length; i++) {
            activeLinks[i].className = activeLinks[i].className.replace(/\b ?active/g, '');
        }
    }

    _addChildrenWithAnchorId() {
        let index = 0;

        return React.Children.map(this.props.children, (child) => {
            let id = this.props.anchors[index];

            index++;

            if (id) {
                return React.cloneElement(child, {
                    id: id
                });
            } else {
                return child;
            }
        });
    }

    _addOverflowToBody() {
        document.querySelector('body').style.overflow = 'hidden';
    }

     _removeOverflowFromBody() {
        document.querySelector('body').style.overflow = 'initial';
    }

    _addMouseWheelEventHandlers() {
        window.addEventListener('mousewheel', this._handleMouseWheel, false);
        window.addEventListener('DOMMouseScroll', this._handleMouseWheel, false);
    }

    _removeMouseWheelEventHandlers() {
        window.removeEventListener('mousewheel', this._handleMouseWheel);
        window.removeEventListener('DOMMouseScroll', this._handleMouseWheel);
    }

    _isElementOverflown(element) {
        return element !== undefined ? element.scrollHeight > element.clientHeight : true;
    }

    _isMouseOverDontScrollElementOverflown(element) {
        return element !== undefined ? element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth : true;
    }

    _isScrolledToPageBottom(element){
        return element !== undefined ? element.scrollTop >= (element.scrollHeight - element.offsetHeight) : false;
    }

    _isScrolledToPageTop(element){
        return element !== undefined ? element.scrollTop <= 0 : false;
    }

    _isMouseOverDontScrollClassBottom(element) {
        return element !== undefined ? element.scrollTop >= (element.scrollHeight - element.offsetHeight) : false; // here we need to check for a className called "dontscroll" if we are over it, return true, otherwise return false.
    }

    _isMouseOverDontScrollClassTop(element){
        return element !== undefined ? element.scrollTop <= 0 : false;
    }

    _handleMouseWheel(event) {
        const e = window.event || event; // old IE support
        
        // 1 === UP | -1 === DOWN
        const delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        const nextSection = this.state.activeSection - delta;
        var parent = document.querySelector("." + this.props.className);
        var currentSection = parent.children[this.state.activeSection];

        if(currentSection && currentSection.className === "Section selective-scrolling"){
            var currentChildSection = document.querySelector("div.scrollmenu");
            // if we need to extend this functionality and have more than one element like this on a single page(or section) we will need to loop through all the children 
            var currentscrollSection = currentChildSection ? currentChildSection.children[0] : undefined;
            let isDontScrollOverflown = currentscrollSection != null ? this._isMouseOverDontScrollElementOverflown(currentscrollSection) : false;
            let isMouseOverDontScroll = isDontScrollOverflown && delta === -1 ? this._isMouseOverDontScrollClassBottom(currentscrollSection) : isDontScrollOverflown && delta === 1 ? this._isMouseOverDontScrollClassTop(currentscrollSection) : false;
        }
        
        let isOverflown = currentSection != null ? this._isElementOverflown(currentSection) : false;
        let isScrolled = isOverflown && delta === -1 ? this._isScrolledToPageBottom(currentSection) : isOverflown && delta === 1 ? this._isScrolledToPageTop(currentSection) : false;
       
        //if(currentSection === null) {parent.children[this.state.activeSection]}

        if (this.state.scrollingStarted || nextSection < 0 || this._childrenLength === nextSection || (isOverflown && !isScrolled)) {
            return false;
        }

        this._setAnchor(nextSection);
        this._handleSectionTransition(nextSection);
        this._addActiveClass();
    }

    onMove(e){
        if(e && e !== undefined){
            //var ct = e.target.className;
            // perform any action here that should happen during a move action
        }
    }

    _handleResize() {
        const position = 0 - (this.state.activeSection * window.innerHeight);

        this.setState({
            scrollingStarted: true,
            windowHeight: window.innerHeight,
            sectionScrolledPosition: position,
            delay: 0
        });

        this._resetScroll();
    }

    _handleSectionTransition(index) {
        const position = 0 - (index * this.state.windowHeight);
        const that = this;

        if (!this.props.anchors.length || index === -1 || index >= this.props.anchors.length) {
            return false;
        }

        this.setState({
            scrollingStarted: true,
            activeSection: index,
            sectionScrolledPosition: position
        });

        var parent = document.querySelector("." + this.props.className);
        var currentSection = parent && parent.children && parent.children[index];
        if(currentSection){ currentSection.scrollTop = 0; }

        setTimeout(function(){
            that._resetScroll();
            that._handleScrollCallback();
        }, 200);
    }

    _handleArrowKeys(e) {
        const event = window.event ? window.event : e;
        const activeSection = event.keyCode === 38  ? this.state.activeSection - 1 : (event.keyCode === 40 ? this.state.activeSection + 1 : -1);

        if (this.state.scrollingStarted || activeSection < 0 || this._childrenLength === activeSection) {
            return false;
        }

        this._setAnchor(activeSection);
        this._handleSectionTransition(activeSection);
        this._addActiveClass();
    }
    
    _handleTouchNav() {
    var that = this;

    var touchsurface = document.querySelector("." + this.props.className),
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 50, //required min distance traveled to be considered swipe
    restraint = 100, // maximum distance allowed at the same time in perpendicular direction
    allowedTime = 1000, // maximum time allowed to travel that distance
    elapsedTime,
    startTime,
    handleswipe = function(swipedir){console.log(swipedir);}
  
    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime() // record time when finger first makes contact with surface
    }, false)
  
    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime // get time elapsed
        if (elapsedTime <= allowedTime){ // first condition for awipe met
            if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
              swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
              var direction = swipedir === 'down' ? that.state.activeSection - 1 : swipedir === 'up' ? that.state.activeSection + 1 : -1;
              
              // if the page is overflown and we are not finished scrolling... we dont change sections
              var parent = document.querySelector("." + that.props.className);
              var currentSection = parent.children[that.state.activeSection];
              let isOverflown = currentSection != null ? that._isElementOverflown(currentSection) : false;
              //on mobile... swipe down means scrolling up and swipe up means scrolling down
              let isScrolled = isOverflown && swipedir === 'up' ? that._isScrolledToPageBottom(currentSection) : isOverflown && swipedir === 'down' ? that._isScrolledToPageTop(currentSection) : false;
              if (isOverflown && !isScrolled) return false;
              
              var hash = that.props.anchors[direction];

              if (!that.props.anchors.length || hash) {
                window.location.hash = '#' + hash;
              }

              that._handleSectionTransition(direction);
            }
        }
        handleswipe(swipedir)
    }, false)
  }

    _handleAnchor() {
        const hash = window.location.hash.substring(1);
        const activeSection = this.props.anchors.indexOf(hash);

        if (this.state.activeSection !== activeSection) {
            this._handleSectionTransition(activeSection);
            this._addActiveClass();
        }
    }

    _setAnchor(index) {
        const hash = this.props.anchors[index];

        if (!this.props.anchors.length || hash) {
            window.location.hash = '#' + hash;
        }
    }

    _handleScrollCallback() {
        if (this.props.scrollCallback) {
            setTimeout(() => this.props.scrollCallback(this.state), 0);
        }
    }

    _resetScroll() {
        this._clearResetScrollTimer();

        this._resetScrollTimer = setTimeout(() => {
            this.setState({
                scrollingStarted: false,
                delay:  this.props.delay
            });
        });
    }

    _clearResetScrollTimer() {
        if (this._resetScrollTimer) {
            clearTimeout(this._resetScrollTimer);
        }
    }
    
    render() {
        let containerStyle = {
            height: '100%',
            width: '100%',
            position: 'relative',
            transform: `translate3d(0px, ${this.state.sectionScrolledPosition}px, 0px)`,
            transition: `all ${this.state.delay}ms ease`,
        };
        return (
            <div>
                <div className={this.props.className} style={containerStyle} onMouseOver={this.onMove}>
                    {this.props.scrollBar ? this._addChildrenWithAnchorId() : this.props.children}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { dispatch, activesection } = state;
    return {
        dispatch, activesection
    };
}

const connectedCustomFullPageContainer = connect(mapStateToProps)(CustomFullPageContainer);
export { connectedCustomFullPageContainer as CustomFullPageContainer };