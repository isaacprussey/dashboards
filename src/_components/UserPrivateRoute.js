import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const authCheck = {
    passUserInspection(user) {
        if(user){
            user = JSON.parse(user);
            if(user && user.loggedIn === true) return true;
            return false
        } 
        return false
    }
}

export const UserPrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        sessionStorage.getItem('user') && authCheck.passUserInspection(sessionStorage.getItem('user')) === true
    ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)