import 'babel-polyfill';
import React, { Component } from 'react';
import { Router, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { loadProgressBar } from 'axios-progress-bar'
import { LoginPage } from '../LoginPage';
import { UserLanding } from '../UserLanding';
import { UserPrivateRoute } from '../_components';
import 'axios-progress-bar/dist/nprogress.css'

class App extends Component {
  constructor(props) {
    super(props);

    history.listen((location, action) => {
      // as we add routes, we may need to do things here during histroy entries
    });
  }
  componentDidMount(){
    loadProgressBar(); // loads our progress bar that will activate during axios events
  }
  render() {
    return (
      <div className="siteContainer">
                <Router history={history}>
                    <Route render={({ location }) => (
                        <div>                         
                        <TransitionGroup>
                            <CSSTransition key={location.key} timeout={600} classNames="fade" mountOnEnter={true} unmountOnExit={true} transitionEnter={true}>
                            <Switch location={location}>
                                <Route exact path="/login" render={(props) => (<LoginPage {...props} />)}/>
                                <UserPrivateRoute exact path="/" component={UserLanding} />
                            </Switch>
                            </CSSTransition>
                        </TransitionGroup>
                        </div>
                    )}/>
                </Router>
            </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
      user
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 
