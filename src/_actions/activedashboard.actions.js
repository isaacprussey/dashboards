import { activeDashboardConstants, userConstants } from '../_constants';
import { activeDashboardService } from '../_services';
import { userActions } from '.';

export const activeDashboardActions = {
    set,
    getInitialData
};

function set(user, activedashboard) {
    return dispatch => {
     return new Promise(function(resolve, reject) {
        activeDashboardService.set(activedashboard)
            .then(
                activedashboard => {
                    dispatch(set(activedashboard));
                    return resolve(activedashboard);
                },
                error => {
                    return reject(dispatch(failure(error)));
                }
            );
        });
        function set(activedashboard) { return { type: activeDashboardConstants.SET, activedashboard } }
        function failure(error) { return { type: activeDashboardConstants.FAILURE, error } }
    }
}


function getInitialData(user, activedashboard){
    return dispatch => {
            // Remove all session data, if it exists and would be new | refresh if permissions
            if(sessionStorage.getItem('welcome') !== null && sessionStorage.getItem('welcome').dashboardId !== activedashboard.id){ 
                sessionStorage.removeItem('welcome'); 
                dispatch(clearWelcome());
                dispatch(userActions.welcome(user, activedashboard));
            }
            if(sessionStorage.getItem('dashboard') !== null && sessionStorage.getItem('dashboard').dashboardId !== activedashboard.id){ 
                sessionStorage.removeItem('dashboard');  dispatch(clearDashboard());
                dispatch(userActions.dashboard(user, activedashboard));
            }
    };

    function clearWelcome() { return { type: userConstants.WELCOME_CLEAR } }
    function clearDashboard() { return { type: userConstants.DASHBOARD_CLEAR } }
}

        