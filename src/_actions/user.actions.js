import { userConstants, activeDashboardConstants } from '../_constants';
import { userService } from '../_services';
import { activeDashboardActions } from '.';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    welcome,
    dashboard,
};

function login(Username, Password) {
    return dispatch => {
        dispatch(request({ Username }));
        userService.login(Username, Password)
            .then(
                user => { 
                    dispatch(success(user));
                    if(user){
                        dispatch(activeDashboardActions.set(user, user.dashboards[0])).then(() => {
                            dispatch(activeDashboardActions.getInitialData(user, user.dashboards[0]));
                            history.push('/');
                        });
                    } 
                    else {
                        let error = "User account is lacking permissions";
                        dispatch(failure(error));
                    }
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    return dispatch => {
        sessionStorage.removeItem('siteMessageHistory');
        dispatch(clearWelcome());
        dispatch(clearDashboard());
        dispatch(clearActiveSection());
        dispatch(clearActiveDashboard());
        dispatch(clearAuthentication());
    } 
    function clearWelcome() { return { type: userConstants.WELCOME_CLEAR } }
    function clearDashboard() { return { type: userConstants.DASHBOARD_CLEAR } }
    function clearActiveDashboard() { return { type: activeDashboardConstants.CLEAR } }
    function clearActiveSection() { return { type: userConstants.ACTIVESECTION_CLEAR } }
    function clearAuthentication() { return { type: userConstants.LOGOUT } }
}

function welcome(user, activedashboard) {
    return dispatch => {
        if(user && activedashboard){
            dispatch(request({ dashboardId: activedashboard.id }));
            userService.welcome(user, activedashboard)
            .then(
                welcome => { 
                    dispatch(success(welcome));
                },
                error => {
                    dispatch(failure(error));
                }
            );
        }
    };

    function request(welcome) { return { type: userConstants.WELCOME_REQUEST, welcome } }
    function success(welcome) { return { type: userConstants.WELCOME_SUCCESS, welcome } }
    function failure(error) { return { type: userConstants.WELCOME_FAILURE, error } }
}

function dashboard(user, activedashboard) {
    return dispatch => {
        if(user && activedashboard){
        dispatch(request({ dashboardId: activedashboard.id }));
        userService.dashboard (user, activedashboard)
            .then(
                dashboard => { 
                    dispatch(success(dashboard));
                },
                error => {
                    dispatch(failure(error));
                }
            );
        }
    };

    function request(dashboard) { return { type: userConstants.DASHBOARD_REQUEST, dashboard } }
    function success(dashboard) { return { type: userConstants.DASHBOARD_SUCCESS, dashboard } }
    function failure(error) { return { type: userConstants.DASHBOARD_FAILURE, error } }
}