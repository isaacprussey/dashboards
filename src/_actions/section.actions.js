import { userConstants, sectionConstants } from '../_constants';

export const sectionActions = {
    set
};

function set(activesection) {
    if(activesection === sectionConstants.WELCOME ||
        activesection === sectionConstants.DASHBOARD){
            return { type: userConstants.ACTIVESECTION_SUCCESS, activesection };
    } else {
        return { type: userConstants.ACTIVESECTION_FAILURE };
    }
}

