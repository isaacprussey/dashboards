import { config } from '../_helpers';
import axios from 'axios';

export const userService = {
    login,
    welcome,
    dashboard,
};

function login(Username, Password) {

    return new Promise(
        function (resolve, reject) {
             // for the sample, this is an example of a server answer
            let sampleActivedashboard = {
                id:'c71fba5a-c788-4566-b67b-c18a3a882630',
                data: 'Sample Data to display'
            };

            let sampleUserData = {
                id: 'f14bf884-8986-4da4-8d42-63b93ee19f4b',
                dashboards: [sampleActivedashboard],
                loggedIn: true,
                // the token is used to identify the user to the server, we would get it after logging in
                token: 'JpT2SVHxmUytrfP3PzXTMw=4b355aqmzvN0LlEqPLe5ahbk5oQ-8782eaff87382e8e-3f29-4056-b5cd-c467cd7ff491',
                firstName: 'Test First Name',
                lastName: 'Test Last Name',
            };

            sessionStorage.setItem('user', JSON.stringify(sampleUserData));
            sessionStorage.setItem('activedashboard', JSON.stringify(sampleActivedashboard));

            return resolve(sampleUserData);
        }
    );
   
    // for use with an api, you may use the below call
    /*
    return axios({
        method: 'post',
        url: config.apiUrl + '/api/Login/Authenticate',
        data: {
            Username: Username,
            Password: Password
        },
        headers: { 'Content-Type': 'application/json' },
      }).then(handleResponse, handleError)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                sessionStorage.setItem('user', JSON.stringify(user.data));
                if(user.data.dashboards.length > 0) {
                    let tempActivedashboard = user.data.dashboards[0];      
                    sessionStorage.setItem('activedashboard', JSON.stringify(tempActivedashboard));
                }
            }
            return user.data;
        });
        */
}

function welcome(user, activedashboard) {
    if(user && user.id && activedashboard && activedashboard.id)
    {
        return new Promise(
            function (resolve, reject) {
                let sampleWelcome = {
                    id: '3fe8923f-7381-46e8-a955-2531793a33cc',
                    data: 'Sample Welcome Data'
                };
        
                sessionStorage.setItem('welcome', JSON.stringify(sampleWelcome));
                return resolve(sampleWelcome);
            }
        );
       
    /*
    return axios({
        method: 'get',
        url: config.apiUrl + '/api/Welcome/' + activedashboard.id,
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + user.token },
      }).then(handleResponse, handleError)
        .then(welcome => {
            if (welcome) {
                // store welcome details and jwt token in local storage to keep user logged in between page refreshes
                sessionStorage.setItem('welcome', JSON.stringify(welcome.data));
                return welcome.data;
            }
            return welcome;
    });  */
    }
  
}

function dashboard(user, activedashboard) {
    if(user && user.id && activedashboard && activedashboard.id){
        return new Promise(
            function (resolve, reject) {
                let sampleDashboard = {
                    id: '5e58ef82-8792-4c81-bc5d-a7a54cbf86c0',
                    data: 'Sample Dashboard Data'
                };
                sessionStorage.setItem('dashboard', JSON.stringify(sampleDashboard));
                return resolve(sampleDashboard);
            }
        );
       
    /*
    return axios({
        method: 'get',
        url: config.apiUrl + '/api/Dashboard/' + activedashboard.id,
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + user.token },
      }).then(handleResponse, handleError)
        .then(dashboard => {
            if (dashboard) {
                // store dashboard details and jwt token in local storage to keep user logged in between page refreshes
                //sessionStorage.setItem('dashboard', JSON.stringify(dashboard.data));
                return dashboard.data;
            }
            return {};
        });
    */
    }
}

function handleResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.status === 200) { // 200 means okay
            // return json if it was returned in the response
            var contentType = response.headers["content-type"];
            if (contentType && contentType.includes("application/json")) {
                resolve(response);
            } else {
                resolve();
            }
        } else {
            // return error message from response body
            reject(response.statusText);
        }
    });
}

function handleError(error) {
    return Promise.reject(error && error.message);
}
