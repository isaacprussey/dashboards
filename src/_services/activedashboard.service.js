export const activeDashboardService = {
    set,
};

function set(activedashboard) {
    return new Promise(function(resolve, reject) {
        if(activedashboard && activedashboard.id){
            sessionStorage.setItem('activedashboard', JSON.stringify(activedashboard));
            return resolve(activedashboard);
        } else {
            reject('There was a problem finding your dashboard');
        }
      });
}
