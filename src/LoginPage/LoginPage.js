import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { ContentContainer } from '../_components';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        
        // reset login status
        dispatch(userActions.logout());

        this.state = {
            Username: '',
            Password: '',
            Submitted: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { Username, Password } = this.state;
        const { dispatch } = this.props;
        if (Username && Password) {
            dispatch(userActions.login(Username, Password));
        }
    }
   
    render() {
        const { Username, Password, Submitted } = this.state;
        const loginBoxStyle = {
            minWidth: '320px',
            maxWidth: '400px',
            height: 'auto',
            backgroundColor: 'rgba(8, 0, 0, .4)',
            padding: '40x 40px',
            margin: '0 auto 25px',
            marginTop: '200px',
            borderRadius: '0',
            boxShadow: '5px 5px 5px rgba(0, 0, 0, 0.3)',
        }

        return (
            <ContentContainer className="loginpage">
            <div style={loginBoxStyle}>
                <div>
                    <img alt="Isaac Logo" src="is.svg" style={{height: '200px', width: 'auto'}} />
                </div>
                <div className=""> 
                    <span className="loginheader animation-box" >Login to Sample :)</span>
                </div>
                <form name="form" onSubmit={this.handleSubmit}>
                <div className="loginform">
                    <div className={'logindiv' + (Submitted && !Username ? ' has-error' : '')} >
                        <input className="logintextbox" type="text" size="35" placeholder="Username" autoFocus name="Username" value={Username} onChange={this.handleChange} />
                        {Submitted && !Username &&
                            <div className="help-block" >A username would REALLY help... just saying</div>
                        }
                    </div>
                    <div  className={'logindiv' + (Submitted && !Password ? ' has-error' : '')} >
                        <input className="logintextbox" type="Password" size="35" placeholder="Password" name="Password" value={Password} onChange={this.handleChange} />
                        {Submitted && !Password &&
                            <div className="help-block" >We need a password...</div>
                        }
                    </div>
                </div>
                <div className="loginform">
                        <div className="">
                            <button className="button button1">Log in</button>
                        </div>
                </div>
                </form>
                </div>
            </ContentContainer>
            );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 