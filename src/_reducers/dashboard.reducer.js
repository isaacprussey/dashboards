import { userConstants } from '../_constants';

let tempState = JSON.parse(sessionStorage.getItem('dashboard'));
const initialState = tempState ? tempState : {};

export function dashboard(state = initialState, action) {
  switch (action.type) {
    case userConstants.DASHBOARD_REQUEST:
      return action.dashboard;
    case userConstants.DASHBOARD_SUCCESS:
      return action.dashboard;
    case userConstants.DASHBOARD_FAILURE:
      return {};
    case userConstants.DASHBOARD_CLEAR:
      if(sessionStorage.getItem('dashboard') !== null){ sessionStorage.removeItem('dashboard'); }
      return {};
    default:
      return state
  }
}