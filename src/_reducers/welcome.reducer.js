import { userConstants } from '../_constants';

let tempState = JSON.parse(sessionStorage.getItem('welcome'));
const initialState = tempState ? tempState : {};

export function welcome(state = initialState, action) {
  switch (action.type) {
    case userConstants.WELCOME_REQUEST:
      return action.welcome;
    case userConstants.WELCOME_SUCCESS:
      return action.welcome;
    case userConstants.WELCOME_FAILURE:
      return {};
    case userConstants.WELCOME_CLEAR:
      if(sessionStorage.getItem('welcome') !== null){ sessionStorage.removeItem('welcome'); }
      return {};
    default:
      return state
  }
}