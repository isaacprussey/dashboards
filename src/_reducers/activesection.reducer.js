import { userConstants, sectionConstants } from '../_constants';

let tempState = JSON.parse(sessionStorage.getItem('activesection'));
const initialState = tempState ? tempState : sectionConstants.NOTSET;

export function activesection(state = initialState, action) {
  switch (action.type) {
    case userConstants.ACTIVESECTION_REQUEST:
      return action.activesection;
    case userConstants.ACTIVESECTION_SUCCESS:
      return action.activesection;
    case userConstants.ACTIVESECTION_FAILURE:
      return action.activesection;
    case userConstants.ACTIVESECTION_CLEAR:
      if(sessionStorage.getItem('activesection') !== null){ sessionStorage.removeItem('activesection'); }
      return sectionConstants.NOTSET;
    default:
      return state
  }
}