import { combineReducers } from 'redux';
import { authentication } from './authentication.reducer';
import { welcome } from './welcome.reducer';
import { dashboard } from './dashboard.reducer';
import { activedashboard } from './activedashboard.reducer';
import { activesection } from './activesection.reducer';

const rootReducer = combineReducers({
  authentication,
  activedashboard,
  welcome,
  dashboard,
  activesection,
});

export default rootReducer;