import { activeDashboardConstants } from '../_constants';

let tempActiveDashboard = sessionStorage.getItem('activedashboard');
let tempState = typeof tempActiveDashboard === "object" ? tempActiveDashboard : JSON.parse(tempActiveDashboard);
const initialState = tempState ? tempState : {};

export function activedashboard(state = initialState, action) {
  switch (action.type) {
    case activeDashboardConstants.SET:
      return action.activedashboard;
    case activeDashboardConstants.CLEAR:
      if(sessionStorage.getItem('activedashboard') !== null){ sessionStorage.removeItem('activedashboard'); }
      return {};
    case activeDashboardConstants.FAILURE:
      return {};
    default:
      return state
  }
}